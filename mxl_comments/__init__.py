from django.contrib.comments.models import Comment
from mxl_comments.forms import LightCommentForm

def get_model():
    return Comment

def get_form():
    return LightCommentForm

