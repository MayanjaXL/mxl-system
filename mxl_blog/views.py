from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from mxl_blog.models import BlogPost, Blog, Tag
from mxl_system.auth.permission_mixin import PermissionMixin
from mxl_system.generic_views import MListView, MSimpleListView
from django.conf import settings


#- BLOG SYSTEM VARIABLES
#- blog_name:       the name of the blog
#- blog_slug:       the slug of the blog
#- blog_title:      the title of the blog

#-----------------------------------------------------------------------------------------------------------------------
class Blog_View(TemplateView):
    """
    It is not sure what this class does
    """
    template_name = "mxl_blog_blog_post.html"

    def get_context_data(self, **kwargs):
        context = super(Blog_View, self).get_context_data(**kwargs)
        return context

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostView(DetailView):
    """
    Returns a single blog post
    """
    template_name = "b_blog_post.html"
    model = BlogPost
    get_related = True
    related_posts = 15
    order_by = "-date_updated"

    def get(self, request, **kwargs):

        #If a shortcut_slug was passed, use it
        if 'shortcut_slug' in kwargs:
            self.shortcut_slug = kwargs['shortcut_slug']
        else:
            self.shortcut_slug = None


        #Get the object (blog post)
        self.object = self.get_object()

        if self.object is None:
            raise Http404

        #Handle authentication - authenticated users only
        if self.object.blog.authentication_required:
            if not request.user.is_authenticated():
                return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        #Handle authentication - groups only
        if not PermissionMixin.user_in_group(request.user, self.object.blog.read_access.all()):
            raise PermissionDenied


        #Handle template - or use the default if none is provided
        if self.object.blog.post_template is not None:
            self.template_name = self.object.blog.post_template.path

        if self.object.post_template is not None:
            self.template_name = self.object.post_template.path

        return super(BlogPostView, self).get(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BlogPostView, self).get_context_data(**kwargs)

        #Determine whether the posted-by line should be displayed or not, based on whether it is an article or not
        context['show_postedline'] = self.object.blog_post_class.treat_as_article == False

        #Set blog context variables
        context['blog_name'] = self.object.blog.name
        context['blog_slug'] = self.object.blog.slug
        context['blog'] = self.object.blog
        self.get_blogpost_related(context)
        return context

    def get_object(self, queryset=None):

        #Find the blog post by its shortcut_slug, if one is defined, or otherwise use the
        #default mechanism
        if self.shortcut_slug:
            return BlogPost.objects.get(shortcut_slug=self.shortcut_slug)
        else:
            return super(BlogPostView, self).get_object()

    def get_blogpost_related(self, context):
        """
        Gets related items for the blog post. These include:
        - Past x posts for the blog the post belongs to
        - Keywords for the blog the post belongs to
        """

        blog_id = self.object.blog.id

        #1 - Related posts -these are posts in this blog
        context['related_posts'] = BlogPost.objects.filter(blog__id = blog_id).order_by(self.order_by)[:self.related_posts]

        #2 - Tags in use by this blog
        context['blog_tags'] = Tag.objects.raw("SELECT * FROM mxl_blog_tag WHERE mxl_blog_tag.ID IN "
                                                "(SELECT tag_id FROM mxl_blog_blogpost_tags "
                                                "INNER JOIN mxl_blog_blogpost bp ON blogpost_id = bp.ID "
                                                "WHERE blog_id = %s)" % blog_id)

        return


#-----------------------------------------------------------------------------------------------------------------------
class BlogPostListView(MSimpleListView):

    paginate_by = 50
    template_name = "b_blogpost_list.html"
    model = BlogPost

    def get(self, request, *args, **kwargs):
        self.blog_slug = kwargs['blog_slug']
        self.blog = get_object_or_404(Blog, slug = self.blog_slug, allow_listing = True)
        self.blog_id = self.blog.id

        #authentication happens here
        if self.blog.authentication_required:
            if not request.user.is_authenticated():
                return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        #Handle authentication - groups only
        if not PermissionMixin.user_in_group(request.user, self.blog.read_access.all()):
            raise PermissionDenied

        #Handle template - or use the default if none is provided
        if self.blog.list_template is not None:
            self.template_name = self.blog.list_template.template.path

        return super(BlogPostListView, self).get(request, *args, **kwargs)

    def get_queryset(self):

        return BlogPost.objects.filter(blog__id = self.blog_id)

    def get_context_data(self, **kwargs):
        context = super(BlogPostListView, self).get_context_data(**kwargs)
        context['info'] = self.blog_slug

        #Set contect variables
        context['blog'] = self.blog
        context['blog_name'] = self.blog.name
        context['blog_slug'] = self.blog.slug
        return context


