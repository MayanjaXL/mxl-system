from django import template
from django.core import urlresolvers
from django.template.base import TemplateSyntaxError
from mxl_blog.models import *
from django.db.models import Q
from mxl_system.DbReader import *
from blog_nodes import  *

#-----------------------------------------------------------------------------------------------------------------------
register = template.Library()

#-----------------------------------------------------------------------------------------------------------------------
@register.inclusion_tag("t_categorylist.html", takes_context=True)
def render_category_list(context):
    pass
    #categories = Category.objects.all()
    #return locals()

#-----------------------------------------------------------------------------------------------------------------------
@register.inclusion_tag("mxl_blog_taglist.html", takes_context=True)
def render_tag_list(context):
    pass
    #tags = Tag.objects.all()
    #return locals()

#-----------------------------------------------------------------------------------------------------------------------
@register.tag
def get_tag_list(parser, token):

    tags = Tag.objects.all()
    return locals()

#-----------------------------------------------------------------------------------------------------------------------
def get_latest(parser, token):
    bits = token.contents.split()
    if len(bits) != 5:
        raise TemplateSyntaxError, "get_latest tag takes exactly four arguments"
    if bits[3] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"
    return DbListReader(bits[1], bits[2], bits[4])

#-----------------------------------------------------------------------------------------------------------------------
def get_blog_category_list(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise TemplateSyntaxError, "get_blog_category_list exactly two arguments"
    if bits[1] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"
    return DbListReader("mxl_blog.Category", 0, bits[2])

#-----------------------------------------------------------------------------------------------------------------------
def get_blog_tag_list(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise TemplateSyntaxError, "get_blog_tag_list exactly two arguments"
    if bits[1] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"
    return DbListReader("mxl_blog.Tag", 0, bits[2], "name")

#-----------------------------------------------------------------------------------------------------------------------
def all_blog_post_list(parser, token):
    """
    Returns a list of blog posts
    """
    bits = token.contents.split()
    if len(bits) != 4:
        raise TemplateSyntaxError, "get_blog_post_list exactly three arguments"
    if bits[2] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"
    return DbListReader("mxl_blog.BlogPost", bits[1], bits[3], "-date_updated")

#-----------------------------------------------------------------------------------------------------------------------
def blog_post_list(parser, token):

    """
    Returns a list of blog posts, for a specific blog, based on the blog id (bits[1])
    """
    bits = token.contents.split()
    #bits one should be the current blog post
    if len(bits) != 5:
        raise TemplateSyntaxError, "get_blog_post_list exactly three arguments"
    if bits[3] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"

    return BlogPostListNode(bits[1], bits[2], bits[4], "-date_updated")

#-----------------------------------------------------------------------------------------------------------------------
def slug_blog_post_list(parser, token):

    """
    Returns a list of blog posts, for a specific blog, based on the blog slug (bits[1])
    slug_blog_post_list slug number_of_items as output
    """
    bits = token.contents.split()
    #bits one should be the current blog post
    if len(bits) != 5:
        raise TemplateSyntaxError, "get_blog_post_list exactly three arguments"
    if bits[3] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"

    return BlogPostListByBlogSlugNode(bits[1], bits[2], bits[4], "-date_updated")

#-----------------------------------------------------------------------------------------------------------------------
def blog_post(parser, token):
    """
    Returns a blog post based on its slug
    """
    bits = token.contents.split()
    if len(bits) != 4:
        raise TemplateSyntaxError, "blog_post expects exactly three arguments"
    if bits[2] != 'as':
        raise TemplateSyntaxError, "third argument to the get_latest tag must be 'as'"

    print "BLOG POST"
    return BlogPostBySlugNode(bits[1], bits[3])

#-----------------------------------------------------------------------------------------------------------------------
@register.inclusion_tag("t_blog_admintags.html", takes_context=True)
def admintags(context, page):

    user = context['request'].user
    auth = user.is_authenticated()

    can_edit = user.has_perm('mxl_blog.change_blogpost')
    change_url = urlresolvers.reverse('admin:mxl_blog_blogpost_change', args=(page.id,))
    delete_url = urlresolvers.reverse('admin:mxl_blog_blogpost_delete', args=(page.id,))

    return locals()


#Register the tag
get_latest = register.tag(get_latest)
get_latest = register.tag(get_blog_category_list)
get_latest = register.tag(all_blog_post_list)
get_latest = register.tag(blog_post_list)
get_latest = register.tag(slug_blog_post_list)
get_latest = register.tag(get_blog_tag_list)
get_latest = register.tag(blog_post)