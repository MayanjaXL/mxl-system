import numbers
from django import template
from django.template.base import TemplateSyntaxError, Node
from mxl_blog.models import *

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostListNode(Node):
    # Generates a list of blog posts based on the blog of a specified blog post

    def __init__(self, blog_post_id, num, varname, orderby=None):
        self.num, self.blog_post_id, self.varname, self.orderby = num, template.Variable(blog_post_id), varname, orderby

    def render(self, context):
        if self.orderby:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context)).order_by(self.orderby)[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context)).order_by(self.orderby)
        else:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context))[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context))

        return ''

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostListByBlogSlugNode(Node):

    def __init__(self, blog_slug, num, varname, orderby=None):
        self.num, self.blog_slug, self.varname, self.orderby = num, template.Variable(blog_slug), varname, orderby
        # Retrieve blog ID from the slug
        self.blog_post_id = Blog.objects.get(slug = self.blog_slug).id

    def render(self, context):
        if self.orderby:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id).order_by(self.orderby)[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id).order_by(self.orderby)
        else:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id)[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id)

        return ''

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostBySlugNode(Node):

    def __init__(self, blog_post_slug, varname):
        print "TEST"
        self.blog_post_slug, self.varname = blog_post_slug, varname

    def render(self, context):
        print "TEST RENER"

        context[self.varname] = BlogPost.objects.get(slug = self.blog_post_slug).content
        return ''


#-----------------------------------------------------------------------------------------------------------------------
class BlogTagListNode(Node):

    def __init__(self, blog_post_id, num, varname, orderby=None):
        self.num, self.blog_post_id, self.varname, self.orderby = num, template.Variable(blog_post_id), varname, orderby

    def render(self, context):

        blog_id = self.blog_post_id.resolve(context)
        context[self.varname] = Tag.objects.raw("SELECT * FROM mxl_django.mxl_blog_tag WHERE mxl_blog_tag.ID IN "
                                                "(SELECT tag_id FROM mxl_blog_blogpost_tags"
                                                "INNER JOIN mxl_blog_blogpost bp ON blogpost_id = bp.ID"
                                                "WHERE blog_id = %s" % blog_id)
        return ""

        if self.orderby:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context)).order_by(self.orderby)[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context)).order_by(self.orderby)
        else:
            if self.num > 0:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context))[:self.num]
            else:
                context[self.varname] = BlogPost.objects.filter(blog__id = self.blog_post_id.resolve(context))

        return ''
