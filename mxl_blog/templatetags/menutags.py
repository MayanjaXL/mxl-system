from django import template
from mxl_blog.models import *
from django.db.models import Q

register = template.Library()


def keep_menuitem(menuitem, user):
    """
    Determines whether a menu item should be visible or not, based on the user's group.
    """
    if not len(menuitem.show_to_groups.all()):
        return True

    if user.is_superuser:
        return True

    for group in menuitem.show_to_groups.all():
        if group in user.groups.all():
            return True

    return False

@register.inclusion_tag("t_menubar.html", takes_context=True)
def menubar(context):
    """
    Creates a tree for the menu bar
    """

    menuitems = []
    finalmenuitems = []
    user = context['request'].user
    auth = user.is_authenticated()

    if auth:
        menuitem_list = MenuItem.objects.filter(parent = None).order_by("order")
    else:
        menuitem_list = MenuItem.objects.filter(parent = None, authentication_required = auth).order_by("order")


    for menuitem in menuitem_list:
        if menuitem.blog_post:
            menuitem.url = "/%s" % menuitem.blog_post.shortcut_slug
            for group in menuitem.show_to_groups.all():
                print group
        menuitems.append(menuitem)

    #Edits the existing list
    menuitems[:] = [tup for tup in menuitems if keep_menuitem(tup, user)]

    if auth:
        childitem_list = MenuItem.objects.filter(~Q(parent = None)).order_by("order")
    else:
        childitem_list = MenuItem.objects.filter(~Q(parent = None), authentication_required = auth).order_by("order")

    #Creates a new list - converted from a queryset
    childitem_list = [tup for tup in childitem_list if keep_menuitem(tup, user)]

    #for c in childitem_list:
    #    for group in c.show_to_groups.all():
    #        print group


    #EXTRA TEXT:
    # 5: HAS CHILDREN
    # 0: HAS NO CHILDREN
    # 3: ONLY SINGLE CHILD
    # 1: FIRST CHILD
    # 2: LAST CHILD

    for parentitem in menuitems:
        childitems = [item for item in childitem_list if item.parent.id == parentitem.id]
        if len(childitems) > 0:
            finalmenuitems.append((5, parentitem)) #has children
        else:
            finalmenuitems.append((0, parentitem)) #no children
        for i, childitem in enumerate(childitems):
            if i == 0 and len(childitems) == 1:
                finalmenuitems.append((2, childitem)) #3: only child
            elif i == 0:
                finalmenuitems.append((1, childitem)) #1: first child
            elif i == len(childitems) -1:
                finalmenuitems.append((2, childitem)) #2 last child
            else:
                finalmenuitems.append((4, childitem)) #middle item

    return locals()
