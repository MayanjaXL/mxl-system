from datetime import datetime
from django.contrib import admin
from models import *
from django import forms

#-----------------------------------------------------------------------------------------------------------------------
class CommonMedia:
    js = (
        'https://ajax.googleapis.com/ajax/libs/dojo/1.6.0/dojo/dojo.xd.js',
        '/static/app-admin/editor.js',
        )
    css = {
        'all': ('/static/app-admin/editor.css',),
        }

#-----------------------------------------------------------------------------------------------------------------------
class BlogAdmin(admin.ModelAdmin):

    list_display  = ('name', 'language', 'slug', 'authentication_required', "admin_description")
    exclude = ('created_by', 'modified_by',)


    def save_model(self, request, obj, form, change):
        if change:
            obj.modified_by = request.user
            #obj.modified_date = datetime.now()
        else:
            obj.created_by = request.user
            #obj.created_date = datetime.now()
        obj.save()

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostAdmin(admin.ModelAdmin):

    list_display  = ('title', 'id', 'blog', 'blog_post_class','published', 'slug', 'category', 'allow_comments', 'modified_by', 'date_updated')
    list_filter = ('category__name', 'tags', 'blog', 'blog_post_class')
    exclude = ('created_by', 'date_created', 'modified_by', 'date_updated')
    #content = forms.CharField(widget=TinyMCE(attrs={'cols': 160, 'rows': 90}))
    #Media = CommonMedia
    #formfield_overrides = {
    #    HtmlEditorModelField: {'widget': TinyMCE(attrs={'cols': 120, 'rows': 45}) }
    #}

    def save_model(self, request, obj, form, change):
        if change:
            obj.modified_by = request.user
            obj.date_updated = datetime.now()
        else:
            obj.created_by = request.user
            obj.date_created = datetime.now()
            obj.modified_by = request.user
            obj.date_updated = datetime.now()
        obj.save()

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostClassAdmin(admin.ModelAdmin):
    list_display = ('name', 'treat_as_article')

class MenuItemAdmin(admin.ModelAdmin):
    list_display  = ('name', 'url', 'parent', 'order')

#-----------------------------------------------------------------------------------------------------------------------
admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogPost, BlogPostAdmin)
admin.site.register(BlogPostClass, BlogPostClassAdmin)
admin.site.register(Language)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Template)
admin.site.register(MenuItem, MenuItemAdmin)