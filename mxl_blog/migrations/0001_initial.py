# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('admin_description', models.CharField(help_text=b'A description/comment that could help you identify this blog', max_length=200, null=True, blank=True)),
                ('slug', models.SlugField(blank=True)),
                ('allow_listing', models.BooleanField(default=False, help_text=b'Determines whether listing of posts is allowed.')),
                ('authentication_required', models.BooleanField(default=False, help_text=b'Determines whether authentication for all posts in this blog is required.')),
                ('created_by', models.ForeignKey(related_name=b'created', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('content', ckeditor.fields.RichTextField()),
                ('published', models.BooleanField(default=False)),
                ('allow_comments', models.BooleanField(default=False)),
                ('allow_comments_until', models.IntegerField(default=7)),
                ('slug', models.SlugField(help_text=b'Generated automatically - the slug by which this post can be retrieved.', blank=True)),
                ('shortcut_slug', models.SlugField(help_text=b'Defines the short slug that can be used when this post is a slug', null=True, blank=True)),
                ('date_created', models.DateTimeField(blank=True)),
                ('date_updated', models.DateTimeField(blank=True)),
                ('blog', models.ForeignKey(to='mxl_blog.Blog')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BlogPostClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('treat_as_article', models.BooleanField(default=False, help_text=b"Treats the page as an article. Article cannot have comments, don't show tages or posted-by lines.")),
                ('description', models.CharField(max_length=255, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True)),
                ('url', models.CharField(help_text=b'Links this menu item to a URL. Do not also set a blog post.', max_length=255, null=True, blank=True)),
                ('authentication_required', models.BooleanField(default=False, help_text=b'Indicates whether this menu item should appear only to authenticated users. This setting does not protect the page.')),
                ('order', models.IntegerField(default=0, help_text=b'Sets the order of the menu item, relative to its neighbours. Lower order numbers appear first.')),
                ('blog_post', models.ForeignKey(blank=True, to='mxl_blog.BlogPost', help_text=b'Links this menu item directly to a blog post. Dot not also set a URL.', null=True)),
                ('parent', models.ForeignKey(blank=True, to='mxl_blog.MenuItem', null=True)),
                ('show_to_groups', models.ManyToManyField(help_text=b'Determines to which groups this menu item should be shown. Do not check to show for all.', related_name=b'auth_groups', to='auth.Group', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('path', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='blogpost',
            name='blog_post_class',
            field=models.ForeignKey(default=1, to='mxl_blog.BlogPostClass'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpost',
            name='category',
            field=models.ForeignKey(default=1, to='mxl_blog.Category'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpost',
            name='created_by',
            field=models.ForeignKey(related_name=b'created2', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpost',
            name='modified_by',
            field=models.ForeignKey(related_name=b'modified2', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpost',
            name='post_template',
            field=models.ForeignKey(blank=True, to='mxl_blog.Template', help_text=b"The template by which this specific post should be rendered. This will override the blog's template.", null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blogpost',
            name='tags',
            field=models.ManyToManyField(to='mxl_blog.Tag', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='language',
            field=models.ForeignKey(to='mxl_blog.Language'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='list_template',
            field=models.ForeignKey(related_name=b'blog_list_template', blank=True, to='mxl_blog.Template', help_text=b'The template by which lists of posts should be rendered.', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='modified_by',
            field=models.ForeignKey(related_name=b'modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='post_template',
            field=models.ForeignKey(related_name=b'blog_post_tempate', blank=True, to='mxl_blog.Template', help_text=b'The template by which individual posts should be rendered.', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='blog',
            name='read_access',
            field=models.ManyToManyField(help_text=b'Determines to which groups posts in this blog may be shown. Do not check to show for all.', related_name=b'blog_read_access', to='auth.Group', blank=True),
            preserve_default=True,
        ),
    ]
