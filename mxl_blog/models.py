from django.db import models
from django.contrib.auth.models import User, Group
from mxl_system.widgets import HtmlEditorModelField
from ckeditor.fields import RichTextField

# Create your models here.
from django.template.defaultfilters import slugify

#-----------------------------------------------------------------------------------------------------------------------
class Language(models.Model):
    name = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class Template(models.Model):
    name = models.CharField(max_length = 50)
    path = models.CharField(max_length = 255)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class Tag(models.Model):
    name = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class Category(models.Model):
    name = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class BlogPostClass(models.Model):
    name = models.CharField(max_length = 50)
    treat_as_article = models.BooleanField(default = False,
        help_text="Treats the page as an article. Article cannot have comments, don't show tages or posted-by lines.")
    description = models.CharField(max_length = 255, blank=True)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class Blog(models.Model):
    name = models.CharField(max_length = 50)
    language = models.ForeignKey(Language)
    admin_description = models.CharField(max_length = 200, blank=True, null=True,
        help_text="A description/comment that could help you identify this blog")
    slug = models.SlugField(blank = True)
    allow_listing = models.BooleanField(default = False,
        help_text="Determines whether listing of posts is allowed.")
    post_template = models.ForeignKey(Template, related_name="blog_post_tempate", blank=True, null=True,
        help_text="The template by which individual posts should be rendered.")
    list_template = models.ForeignKey(Template, related_name="blog_list_template", blank=True, null=True,
        help_text="The template by which lists of posts should be rendered.")
    authentication_required = models.BooleanField(default = False,
        help_text="Determines whether authentication for all posts in this blog is required.")
    created_by = models.ForeignKey(User, null= True, blank = True, related_name="created")
    modified_by = models.ForeignKey(User, null= True, blank = True, related_name="modified")
    read_access = models.ManyToManyField(Group,
        related_name='blog_read_access',
        help_text="Determines to which groups posts in this blog may be shown. Do not check to show for all.",
        blank=True)

    def save(self, *args, **kwargs):
        if not self.id or self.slug == '-':
            self.slug = slugify(self.name)

        super(Blog, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name

#-----------------------------------------------------------------------------------------------------------------------
class BlogPost(models.Model):
    blog = models.ForeignKey(Blog)
    title = models.CharField(max_length = 255)
    content = RichTextField()
    published = models.BooleanField(default = False)
    allow_comments = models.BooleanField(default = False)
    allow_comments_until = models.IntegerField(default = 7)
    tags = models.ManyToManyField(Tag, blank = True)
    category = models.ForeignKey(Category, default = 1)
    blog_post_class = models.ForeignKey(BlogPostClass, default = 1)
    post_template = models.ForeignKey(Template, blank=True, null=True,
        help_text="The template by which this specific post should be rendered. This will override the blog's template.")
    slug = models.SlugField(blank = True,
        help_text="Generated automatically - the slug by which this post can be retrieved.")
    shortcut_slug = models.SlugField(blank = True, null = True,
        help_text="Defines the short slug that can be used when this post is a slug")
    created_by = models.ForeignKey(User, null= True, blank = True, related_name="created2")
    date_created = models.DateTimeField(blank = True)
    modified_by = models.ForeignKey(User, null= True, blank = True, related_name="modified2")
    date_updated = models.DateTimeField(blank = True)

    #-------------------------------------------------------------------------------------------------------------------
    def save(self, *args, **kwargs):
        if not self.id or self.slug == '-':
            self.slug = slugify(self.title)

        super(BlogPost, self).save(*args, **kwargs)

    #-------------------------------------------------------------------------------------------------------------------
    def __unicode__(self):
        return self.title

#-----------------------------------------------------------------------------------------------------------------------
class MenuItem(models.Model):
    name = models.CharField(max_length = 50, null = True)
    url = models.CharField(max_length = 255, null = True, blank = True,
        help_text="Links this menu item to a URL. Do not also set a blog post.")
    blog_post = models.ForeignKey(BlogPost, null = True, blank = True,
        help_text="Links this menu item directly to a blog post. Dot not also set a URL.")
    parent = models.ForeignKey('self', null = True, blank = True)
    authentication_required = models.BooleanField(default = False,
        help_text="Indicates whether this menu item should appear only to authenticated users. This setting does not "\
                  "protect the page.")
    order = models.IntegerField(default = 0, null = False, blank = False,
        help_text="Sets the order of the menu item, relative to its neighbours. Lower order numbers appear first.")
    show_to_groups = models.ManyToManyField(Group,
        related_name='auth_groups',
        help_text="Determines to which groups this menu item should be shown. Do not check to show for all.",
        blank=True)

    #-------------------------------------------------------------------------------------------------------------------
    def __unicode__(self):
        return self.name
