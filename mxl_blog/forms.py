from django import forms
from django.forms.models import ModelForm
from models import *
from django.contrib.comments import Comment

class SimpleCommentForm(ModelForm):

    user_name = forms.CharField()
    comment = forms.CharField(maxlength=500)

    class Meta:
        model = Comment

