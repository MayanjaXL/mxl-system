from django.db import models
import floppyforms as forms

class HtmlEditorModelField(models.TextField):
    pass

class AutocompleteInput(forms.TextInput):
    template_name = 'widget_autocomplete_box.html'

    def get_context(self, name, value, attrs=None, extra_context={}):
        #ctx = super(AutocompleteInput, self).get_context(name, value, attrs, extra_context)
        ctx = super(AutocompleteInput, self).get_context(name, value, attrs)
        #ctx['attrs']['class'] = 'ui-autocomplete-input mxl-autocomplete'
        #ctx['attrs']['autocomplete'] = 'off'
        #ctx['attrs']['role'] = 'textbox'
        #ctx['attrs']['aria-autocomplete'] = 'list'
        #['aria-haspopup'] = 'true'
        ctx['attrs']['data-provide'] = "typeahead"
        ctx['attrs']['data-source'] = '["001", "002"]'
        ctx['attrs']['data-items'] = '4'


        return ctx

class QueryInput(forms.TextInput):

    def get_context(self, name, value, attrs=None, extra_context={}):
        #ctx = super(QueryInput, self).get_context(name, value, attrs, extra_context)
        ctx = super(QueryInput, self).get_context(name, value, attrs)
        ctx['attrs']['class'] = 'mxl-get-query'

        return ctx
