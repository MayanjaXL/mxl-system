from django.template import Library, Node
from django.db.models import get_model

class DbListReader(Node):

    def __init__(self, model, num, varname, orderby=None):
        """
        Constructor - takes model, number to load and the variable name (output)
        """
        self.num, self.varname, self.orderby = num, varname, orderby
        self.model = get_model(*model.split('.'))

    def render(self, context):
        """Render"""

        if self.orderby:
            if self.num > 0:
                context[self.varname] = self.model._default_manager.all().order_by(self.orderby)[:self.num]
            else:
                context[self.varname] = self.model._default_manager.all().order_by(self.orderby)
        else:
            if self.num > 0:
                context[self.varname] = self.model._default_manager.all()[:self.num]
            else:
                context[self.varname] = self.model._default_manager.all()

        return ''







