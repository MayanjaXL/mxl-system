# Create your views here.
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView

def error403(request):
    return render_to_response('s_permission_denied.html')


class PermissionDenied_View(TemplateView):
    template_name = "s_permission_denied.html"
    pass
