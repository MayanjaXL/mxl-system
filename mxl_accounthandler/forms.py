from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.forms.models import ModelForm
import floppyforms as forms
from floppyforms.widgets import PasswordInput, EmailInput
from mxl_system.generic_forms import AjaxForm

class ChangePwdForm(AjaxForm):

    old_password = forms.CharField(widget=PasswordInput)
    new_password = forms.CharField()
    repeat_password = forms.CharField()
    request = None

    def clean_new_password(self):

        data = self.cleaned_data['new_password']
        if len(data) < 6:
            raise forms.ValidationError("The new password should have at least 6 characters.")

        return data

    def clean(self):

        cleaned_data = super(ChangePwdForm, self).clean()
        try:
            old_password = cleaned_data['old_password']
        except KeyError:
            return cleaned_data

        # Check if the passwords match
        try:
            new_password = cleaned_data['new_password']
            repeat_password = cleaned_data['repeat_password']

            if new_password != repeat_password:
                raise forms.ValidationError("New passwords do not match.")
        except KeyError:
            pass

        # Check if the provided current password is correct
        if not self.request.user.check_password(old_password):
            raise forms.ValidationError("The old password provided is invalid.")

        #self.saved_data= cleaned_data
        return cleaned_data

    class Meta:
        title = "Change PWD!"


class ChangeAccountForm(ModelForm):

    #first_name = forms.CharField()
    #last_name = forms.CharField()
    #email = forms.CharField(widget=EmailInput, help_text="Ensure that the email address provided works.")
    #current_password = forms.CharField(widget=PasswordInput, help_text="To protect your account, we ask you to confirm your password.")

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = 'POST'
        self.helper.form_id = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.form_action = ''  # redirect in the view
        self.helper.form_tag = False
        self.helper.help_text_inline = True  # means that I want <span> elements
        super(ChangeAccountForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):

        data = self.cleaned_data['first_name']
        if len(data) < 2:
            raise forms.ValidationError("The name should have at least 2 characters.")

        return data

    class Meta:
        title = "Update account details"
        model = User
        fields = ('first_name', 'last_name', 'email',)