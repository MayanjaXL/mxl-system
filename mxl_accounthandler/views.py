# Create your views here.
from argparse import _
from crispy_forms.helper import FormHelper
from django.conf import settings
from django.contrib.auth.models import User
from django.core.serializers import json
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
import json as simplejson
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from mxl_accounthandler.forms import ChangePwdForm, ChangeAccountForm
import floppyforms as forms
from mxl_system.generic_views import ContextFormView, MUpdateView, MDetailView

def errors_to_json(errors):
    """
    Convert a Form error list to JSON::
    """
    return dict(
        (k, map(unicode, v))
            for (k,v) in errors.iteritems()
    )

class ProfileView(MDetailView):
    template_name = "a_profile.html"
    ajax_template_name = "a_profile_ajax.html"
    success_message = "Your profile was updated."

    def get(self, request, **kwargs):

        if not request.user.is_authenticated:
            return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        self.object = self.get_object()
        return super(ProfileView, self).get(request, **kwargs)

    def get_object(self, queryset=None):

        return self.request.user

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        #form = ChangePwdForm()
        #form.helper = FormHelper()
        #form.helper.form_tag = False
        #context['change_password_form'] = form

        return context

class ChangePasswordView(ContextFormView):

    template_name = "a_changepwd.html"
    form_class = ChangePwdForm
    success_url = "../?//-0/"

    def get(self, request, **kwargs):

        #Ensure a logged on user is doing this
        if not request.user.is_authenticated():
            return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        #If AJAX - use a special form
        if request.GET.get('ajax') is not None:
            #form = self.get_form(self.form_class).helper
            self.template_name = "ajax_form.html"

        return super(ChangePasswordView, self).get(request, **kwargs)

    def get_form_kwargs(self, **kwargs):
        """
        Sets the keywords to be used in the form. This is the proper way to pass parameters to the form.
        """
        kwargs = super(ChangePasswordView, self).get_form_kwargs(**kwargs)
        kwargs['initial']['owner'] = self.request.user
        kwargs['initial']['form_target'] = "accounthandler.changepwd.view"

        if self.request.GET.get('ajax') is not None:
            kwargs['initial']['ajax'] = True
            kwargs['initial']['no-submit'] = True

        return kwargs

    def post(self, request, *args, **kwargs):

        if request.GET.get('ajax') is not None:
            self.template_name = "ajax_form.html"

        self.form_class.request = request
        return super(ChangePasswordView, self).post(request, *args, **kwargs)

    def form_valid(self, form, **kwargs):

        try:
            new_password = form.cleaned_data['new_password']
            self.request.user.set_password(new_password)
            self.request.user.save()
        except:
            if not self.request.is_ajax():
                context = self.get_context_data(**kwargs)
                context["error"] = True
                context["message"] = "The password could not be changed due to an unknown error."
                context['form'] = form
                return self.render_to_response(context)
            else:
                self.message = _("The password could not be changed due to an unknown error.")
                self.data = errors_to_json(form.errors)
                self.success = False
                self.resp = super(ChangePasswordView, self).form_invalid(form)
                payload = {'success': self.success, 'message': self.message, 'data':self.data, 'html':self.resp.rendered_content}
                return HttpResponse(simplejson.dumps(payload), content_type='application/json', )

        #Update was successful
        if not self.request.is_ajax():
            return super(ChangePasswordView, self).form_valid(form)
        else:
            self.message = "Password changed."
            self.success = True
            return_value = {'success': self.success, 'message': self.message}
            return HttpResponse(simplejson.dumps(return_value), content_type='application/json', )


    def form_invalid(self, form):

        if not self.request.is_ajax():
            return super(ChangePasswordView, self).form_invalid(form)
        self.message = _("Validation failed.")
        self.data = errors_to_json(form.errors)
        self.success = False
        self.resp = super(ChangePasswordView, self).form_invalid(form)
        payload = {'success': self.success, 'message': self.message, 'data':self.data, 'html':self.resp.rendered_content}
        return HttpResponse(simplejson.dumps(payload), content_type='application/json', )



#JSON RETURNS
# - message - overal confirmation message of succes or failure
# - success - true or false
# - data - not currently used
# - html - html to display (form)


class ChangeAccountView(ContextFormView):

    template_name = "a_changepwd.html"
    form_class = ChangeAccountForm
    success_url = "../?//-0/"
    success_message = "Your profile was updated."

    def get(self, request, **kwargs):

        #Ensure a logged on user is doing this
        if not request.user.is_authenticated():
            return HttpResponseRedirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

        #If AJAX - use a special form
        if request.GET.get('ajax') is not None:
            #form = self.get_form(self.form_class).helper
            self.template_name = "ajax_form.html"

        return super(ChangeAccountView, self).get(request, **kwargs)

    def get_form_kwargs(self, **kwargs):
        """
        Sets the keywords to be used in the form. This is the proper way to pass parameters to the form.
        """
        print self.request.user.is_authenticated()
        kwargs = super(ChangeAccountView, self).get_form_kwargs(**kwargs)
        kwargs['initial']['owner'] = self.request.user
        kwargs['initial']['form_target'] = "accounthandler.changeaccount.view"
        kwargs['initial']['first_name'] = self.request.user.first_name
        kwargs['initial']['last_name'] = self.request.user.last_name
        kwargs['initial']['email'] = self.request.user.email

        if self.request.GET.get('ajax') is not None:
            kwargs['initial']['ajax'] = True
            kwargs['initial']['no-submit'] = True

        return kwargs


class ChangeAccountView2(MUpdateView):

    model = User
    form_class = ChangeAccountForm
    success_message = "Your profile has been updated."

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse("accounthandler.profile.view")

